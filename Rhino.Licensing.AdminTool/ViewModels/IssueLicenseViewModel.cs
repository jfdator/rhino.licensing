using System;
using Caliburn.PresentationFramework.Screens;
using Rhino.Licensing.AdminTool.Model;
using Rhino.Licensing.AdminTool.Properties;
using Rhino.Licensing.Enums;

namespace Rhino.Licensing.AdminTool.ViewModels
{
    public sealed class IssueLicenseViewModel : Screen
    {
        private License _currentLicense;

        public IssueLicenseViewModel()
        {
            CurrentLicense = new License
            {
                LicenseType = LicenseType.Trial,
                ExpirationDate = DateTime.Now.AddDays(Settings.Default.DefaultTrialDays).Date
            };
        }

        public License CurrentLicense
        {
            get { return _currentLicense; }
            set
            {
                _currentLicense = value;
                NotifyOfPropertyChange(() => CurrentLicense);
            }
        }

        public void Close()
        {
            TryClose(true);
        }
    }
}