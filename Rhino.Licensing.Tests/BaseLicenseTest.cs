using System.Diagnostics.CodeAnalysis;
using System.IO;

namespace Rhino.Licensing.Tests
{
    [SuppressMessage("ReSharper", "AssignNullToNotNullAttribute")]
    public class BaseLicenseTest
    {
        public readonly static string PublicAndPrivate;
        public readonly static string PublicOnly;
        public readonly static string FloatingPrivate;
        public readonly static string FloatingPublic;

        static BaseLicenseTest()
        {
            PublicAndPrivate = new StreamReader(typeof (CanGenerateAndValidateKey)
                                                      .Assembly
                                                      .GetManifestResourceStream(
                                                      "Rhino.Licensing.Tests.public_and_private.xml"))
                .ReadToEnd();

            PublicOnly = new StreamReader(typeof (CanGenerateAndValidateKey)
                                               .Assembly
                                               .GetManifestResourceStream("Rhino.Licensing.Tests.public_only.xml"))
                .ReadToEnd();


            FloatingPrivate = new StreamReader(typeof(CanGenerateAndValidateKey)
                                              .Assembly
                                              .GetManifestResourceStream("Rhino.Licensing.Tests.floating_private.xml"))
               .ReadToEnd();

            FloatingPublic = new StreamReader(typeof(CanGenerateAndValidateKey)
                                              .Assembly
                                              .GetManifestResourceStream("Rhino.Licensing.Tests.floating_public.xml"))
               .ReadToEnd();
        }
    }
}