using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.ServiceModel;
using Rhino.Licensing.Enums;
using Rhino.Licensing.Exceptions;
using Rhino.Licensing.Services;
using Xunit;

namespace Rhino.Licensing.Tests
{
    public class CanUseSubscriptionLicense : BaseLicenseTest
    {
        [Fact]
        [SuppressMessage("ReSharper", "ExceptionNotDocumentedOptional")]
        [SuppressMessage("ReSharper", "ExceptionNotDocumented")]
        public void CanGenerateSubscriptionLicense()
        {
            var generator = new LicenseGenerator(PublicAndPrivate);
            var license = generator.Generate("ayende", new Guid("FFD0C62C-B953-403e-8457-E90F1085170D"),

                new DateTime(2010, 10, 10),
                new Dictionary<string, string> {{"version", "2.0"}}, LicenseType.Subscription);

            var license_header = @"<license id=""ffd0c62c-b953-403e-8457-e90f1085170d"" expiration=""2010-10-10T00:00:00.0000000"" type=""Subscription"" version=""2.0"">";
            var owner_name = "<name>ayende</name>";

            Assert.Contains(license_header, license);
            Assert.Contains(owner_name, license);
        }

        [Fact]
        [SuppressMessage("ReSharper", "ExceptionNotDocumentedOptional")]
        [SuppressMessage("ReSharper", "ExceptionNotDocumented")]
        public void CanValidateSubscriptionLicenseWithTimeToSpare()
        {
            var generator = new LicenseGenerator(PublicAndPrivate);
            var license = generator.Generate("ayende", new Guid("FFD0C62C-B953-403e-8457-E90F1085170D"),

                DateTime.UtcNow.AddDays(15),
                new Dictionary<string, string> { { "version", "2.0" } }, LicenseType.Subscription);
            
            var path = Path.GetTempFileName();
            File.WriteAllText(path, license);


            Assert.DoesNotThrow(() => new LicenseValidator(PublicOnly, path).AssertValidLicense());	
        }

        [Fact]
        [SuppressMessage("ReSharper", "ExceptionNotDocumentedOptional")]
        [SuppressMessage("ReSharper", "ExceptionNotDocumented")]
        public void CanValidateSubscriptionLicenseWithLessThanTwoDaysWhenUrlIsNotAvailable()
        {
            var generator = new LicenseGenerator(PublicAndPrivate);
            var license = generator.Generate("ayende", new Guid("FFD0C62C-B953-403e-8457-E90F1085170D"),
                DateTime.UtcNow.AddDays(1),
                new Dictionary<string, string> { { "version", "2.0" } }, LicenseType.Subscription);


            var path = Path.GetTempFileName();
            File.WriteAllText(path, license);


            Assert.DoesNotThrow(() => new LicenseValidator(PublicOnly, path)
            {
                SubscriptionEndpoint = "http://localhost/"+Guid.NewGuid()
            }.AssertValidLicense());
        }

        [Fact]
        [SuppressMessage("ReSharper", "ExceptionNotDocumentedOptional")]
        [SuppressMessage("ReSharper", "ExceptionNotDocumented")]
        public void CannotValidateExpiredSubscriptionLicenseWhenUrlIsNotAvailable()
        {
            var generator = new LicenseGenerator(PublicAndPrivate);
            var license = generator.Generate("ayende", new Guid("FFD0C62C-B953-403e-8457-E90F1085170D"),
                DateTime.UtcNow.AddDays(-1),
                new Dictionary<string, string> { { "version", "2.0" } }, LicenseType.Subscription);

            var path = Path.GetTempFileName();
            File.WriteAllText(path, license);


            Assert.Throws<LicenseExpiredException>(() => new LicenseValidator(PublicOnly, path)
            {
                SubscriptionEndpoint = "http://localhost/" + Guid.NewGuid()
            }.AssertValidLicense());
        }

        [Fact]
        [SuppressMessage("ReSharper", "ExceptionNotDocumentedOptional")]
        [SuppressMessage("ReSharper", "ExceptionNotDocumented")]
        public void CanValidateExpiredSubscriptionLicenseWhenServiceReturnsNewOne()
        {
            var generator = new LicenseGenerator(PublicAndPrivate);
            var license = generator.Generate("ayende", new Guid("FFD0C62C-B953-403e-8457-E90F1085170D"),

                DateTime.UtcNow.AddDays(-1),
                new Dictionary<string, string> { { "version", "2.0" } }, LicenseType.Subscription);

            var path = Path.GetTempFileName();
            File.WriteAllText(path, license);

            var host = new ServiceHost(typeof(DummySubscriptionLicensingService));
            const string address = "http://localhost:19292/license";
            host.AddServiceEndpoint(typeof(ISubscriptionLicensingService), new BasicHttpBinding(), address);

            host.Open();

            Assert.DoesNotThrow(() => new LicenseValidator(PublicOnly, path)
            {
                SubscriptionEndpoint = address
            }.AssertValidLicense());


            host.Close();
        }

        [Fact]
        [SuppressMessage("ReSharper", "ExceptionNotDocumentedOptional")]
        [SuppressMessage("ReSharper", "ExceptionNotDocumented")]
        public void CanValidateExpiredSubscriptionLicenseWhenServiceReturnsOldOne()
        {
            var generator = new LicenseGenerator(PublicAndPrivate);
            var license = generator.Generate("ayende", new Guid("FFD0C62C-B953-403e-8457-E90F1085170D"),
                                             DateTime.UtcNow.AddDays(-1),
                                             new Dictionary<string, string> { { "version", "2.0" } }, LicenseType.Subscription);


            var path = Path.GetTempFileName();
            File.WriteAllText(path, license);

            var host = new ServiceHost(typeof(NoOpSubscriptionLicensingService));
            const string address = "http://localhost:19292/license";
            host.AddServiceEndpoint(typeof(ISubscriptionLicensingService), new BasicHttpBinding(), address);

            host.Open();


            Assert.Throws<LicenseExpiredException>(() => new LicenseValidator(PublicOnly, path)
            {
                SubscriptionEndpoint = address
            }.AssertValidLicense());


            host.Close();
        }
    }
}