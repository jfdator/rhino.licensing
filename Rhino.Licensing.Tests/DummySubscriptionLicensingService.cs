using System;
using System.IO;
using Rhino.Licensing.Enums;
using Rhino.Licensing.Services;

namespace Rhino.Licensing.Tests
{
    public class DummySubscriptionLicensingService : ISubscriptionLicensingService
    {
        public string LeaseLicense(string previousLicense)
        {
            var tempFileName = Path.GetTempFileName();
            try
            {
                var stringLicenseValidator = new StringLicenseValidator(BaseLicenseTest.PublicOnly, previousLicense);
                if (stringLicenseValidator.TryLoadingLicenseValuesFromValidatedXml() == false)
                    throw new InvalidOperationException("Invalid license provided");
                return new LicenseGenerator(BaseLicenseTest.PublicAndPrivate).Generate(stringLicenseValidator.Name,
                    stringLicenseValidator.UserId,
                    DateTime.UtcNow.AddDays(15),
                    stringLicenseValidator.LicenseAttributes,
                    LicenseType.Subscription);
            }
            finally
            {
                File.Delete(tempFileName);
            }
        }
    }
}