using Rhino.Licensing.Services;

namespace Rhino.Licensing.Tests
{
    public class NoOpSubscriptionLicensingService : ISubscriptionLicensingService
    {
        public string LeaseLicense(string previousLicense)
        {
            return previousLicense;
        }
    }
}