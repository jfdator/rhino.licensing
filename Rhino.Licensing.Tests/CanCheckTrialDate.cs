using System.Diagnostics.CodeAnalysis;
using System.IO;
using Rhino.Licensing.Exceptions;
using Rhino.Licensing.Services;
using Xunit;

namespace Rhino.Licensing.Tests
{
    [SuppressMessage("ReSharper", "ExceptionNotDocumented")]
    public class CanCheckTrialDate : BaseLicenseTest
    {
        [Fact]
        public void WillTellThatWeAreInInvalidState()
        {
            var validator = new LicenseValidator(PublicOnly, Path.GetTempFileName());
            Assert.Throws<LicenseNotFoundException>(() => validator.AssertValidLicense());
        }


        [Fact]
        public void WillFailIfFileIsNotThere()
        {
            var validator = new LicenseValidator(PublicOnly, "not_there");
            Assert.Throws<LicenseFileNotFoundException>(() => validator.AssertValidLicense());
        }
    }
}